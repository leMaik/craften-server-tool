﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Caliburn.Micro;
using Craften_Server_Tool.ViewModels;

namespace Craften_Server_Tool {
    public class CraftenServerToolBootstrapper : BootstrapperBase {
        public CraftenServerToolBootstrapper() {
            Initialize();
        }

        protected override void OnStartup(object sender, System.Windows.StartupEventArgs e) {
            DisplayRootViewFor<MainViewModel>();
        }
    }
}
