﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using Caliburn.Micro;
using Craften_Server_Tool.Api;
using Craften_Server_Tool.Api.Player;

namespace Craften_Server_Tool.ViewModels {
    public class PlayerProfileViewModel : PropertyChangedBase {
        private String _player;

        public String Player {
            get {
                return _player;
            }
            set {
                _player = value;
                UpdatePlayer();
                NotifyOfPropertyChange(() => Player);
            }
        }

        private PlayerInfo _profile;

        public PlayerInfo Profile {
            get { return _profile; }
            set {
                _profile = value;
                NotifyOfPropertyChange(() => Profile);
            }
        }

        private List<SkillViewModel> _skills;
        public List<SkillViewModel> Skills {
            get { return _skills; }
            set {
                _skills = value;
                NotifyOfPropertyChange(() => Skills);
            }
        }

        private SkyblockViewModel _skyblock;
        public SkyblockViewModel Skyblock {
            get { return _skyblock; }
            set {
                _skyblock = value;
                NotifyOfPropertyChange(() => Skyblock);
            }
        }

        private bool _isEditing;
        public bool IsEditing {
            get { return _isEditing; }
            set {
                _isEditing = value;
                NotifyOfPropertyChange(() => IsEditing);

                if (!IsEditing) {
                    Properties.Settings.Default.Player = Player;
                }
            }
        }

        public PlayerProfileViewModel(String player) {
            Player = Properties.Settings.Default.Player;
        }

        private async void UpdatePlayer() {
            try {
                Profile = await CraftenApi.GetPlayer(Player);
                Skills = new List<SkillViewModel>{
                    new SkillViewModel { Name = "Acrobatics", Level = Profile.McMmo.Skills.Acrobatics },
                    new SkillViewModel { Name = "Alchemy", Level = Profile.McMmo.Skills.Alchemy },
                    new SkillViewModel { Name = "Archery", Level = Profile.McMmo.Skills.Archery },
                    new SkillViewModel { Name = "Axes", Level = Profile.McMmo.Skills.Axes },
                    new SkillViewModel { Name = "Excavation", Level = Profile.McMmo.Skills.Excavation },
                    new SkillViewModel { Name = "Fishing", Level = Profile.McMmo.Skills.Fishing},
                    new SkillViewModel { Name = "Herbalism", Level = Profile.McMmo.Skills.Herbalism },
                    new SkillViewModel { Name = "Mining", Level = Profile.McMmo.Skills.Mining },
                    new SkillViewModel { Name = "Repair", Level = Profile.McMmo.Skills.Repair },
                    new SkillViewModel { Name = "Smelting", Level = Profile.McMmo.Skills.Smelting },
                    new SkillViewModel { Name = "Swords", Level = Profile.McMmo.Skills.Swords },
                    new SkillViewModel { Name = "Taming", Level = Profile.McMmo.Skills.Taming },
                    new SkillViewModel { Name = "Unarmed", Level = Profile.McMmo.Skills.Unarmed },
                    new SkillViewModel { Name = "Woodcutting", Level = Profile.McMmo.Skills.Woodcutting }
                };
                Skyblock = new SkyblockViewModel(Profile.Skyblock);
            }
            catch (ApiException) {
                //TODO Display error message
            }
        }

        public void BeginEditPlayer() {
            IsEditing = true;
        }

        public void StopEditPlayer() {
            IsEditing = false;
        }
    }
}
