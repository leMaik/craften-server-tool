﻿using Caliburn.Micro;
using Craften_Server_Tool.Api.Player;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Craften_Server_Tool.ViewModels {
    public class SkyblockViewModel : PropertyChangedBase {
        private ObservableCollection<IslandViewModel> _islands;
        public ObservableCollection<IslandViewModel> Islands {
            get { return _islands; }
            set {
                _islands = value;
                NotifyOfPropertyChange(() => Islands);
            }
        }

        private ObservableCollection<ChallengeViewModel> _challenges;
        public ObservableCollection<ChallengeViewModel> Challenges {
            get { return _challenges; }
            set {
                _challenges = value;
                NotifyOfPropertyChange(() => Challenges);
            }
        }

        public SkyblockViewModel(SkyblockInfo skyblock) {
            Islands = new ObservableCollection<IslandViewModel>(skyblock.Islands.Select((island) => new IslandViewModel(island)));
            Challenges = new ObservableCollection<ChallengeViewModel>(skyblock.Challenges.Select((challenge) => new ChallengeViewModel(challenge.Value)));
        }
    }
}
