﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Craften_Server_Tool.ViewModels {
    public class SkillViewModel {
        public String Name { get; set; }
        public int Level { get; set; }
    }
}
