﻿using Caliburn.Micro;
using Craften_Server_Tool.Api.Player;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Craften_Server_Tool.ViewModels {
    public class ChallengeViewModel : PropertyChangedBase {
        private Challenge _data;
        public Challenge Data {
            get { return _data; }
            set {
                _data = value;
                NotifyOfPropertyChange(() => Data);
            }
        }

        public ChallengeViewModel(Challenge data) {
            Data = data;
        }
    }
}
