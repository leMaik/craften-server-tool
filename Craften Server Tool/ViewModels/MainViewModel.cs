﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using Caliburn.Micro;

namespace Craften_Server_Tool.ViewModels {
    public class MainViewModel : Screen {
        private PlayerProfileViewModel _playerProfile;

        public PlayerProfileViewModel PlayerProfile {
            get { return _playerProfile; }
            set {
                _playerProfile = value;
                NotifyOfPropertyChange(() => PlayerProfile);
            }
        }
        private ServerStatusViewModel _serverStatus;

        public ServerStatusViewModel ServerStatus {
            get { return _serverStatus; }
            set {
                _serverStatus = value;
                NotifyOfPropertyChange(() => ServerStatus);
            }
        }

        protected override void OnInitialize() {
            base.OnInitialize();
            DisplayName = "Craften Server Tool";
            PlayerProfile = new PlayerProfileViewModel("leMaik");
            ServerStatus = new ServerStatusViewModel();
        }

        protected override void OnDeactivate(bool close) {
            base.OnDeactivate(close);

            if (close) {
                Properties.Settings.Default.Save();
            }
        }
    }
}
