﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RestSharp.Deserializers;

namespace Craften_Server_Tool.Api.Server {
    public class ServerStatus {
        [DeserializeAs(Name = "online")]
        public List<String> OnlinePlayers { get; set; }

        public List<Server> Servers { get; set; } 
    }
}
