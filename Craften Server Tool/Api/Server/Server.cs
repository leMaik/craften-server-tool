﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RestSharp.Deserializers;

namespace Craften_Server_Tool.Api.Server {
    public class Server {
        public String Name { get; set; }

        [DeserializeAs(Name = "online")]
        public bool IsOnline { get; set; }

        [DeserializeAs(Name = "tps")]
        public double TicksPerSecond { get; set; }
    }
}
