﻿using System;

namespace Craften_Server_Tool.Api.Player {
    public class PlayerInfo {
        public String Nickname { get; set; }
        public McMmoInfo McMmo { get; set; }
        public EconomyInfo Economy { get; set; }
        public PlotInfo Plots { get; set; }
        public PaintballInfo Paintball { get; set; }
        public SkyblockInfo Skyblock { get; set; }
    }
}
