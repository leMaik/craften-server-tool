﻿namespace Craften_Server_Tool.Api.Player {
    public class PaintballInfo {
        public int HitQuote { get; set; }
        public int Rounds { get; set; }
        public int TeamAttacks { get; set; }
        public int Hits { get; set; }
        public int Defeats { get; set; }
        public double Kd { get; set; }
        public int Money { get; set; }
        public int MoneySpent { get; set; }
        public int Shots { get; set; }
        public int Kills { get; set; }
        public int Points { get; set; }
        public int Draws { get; set; }
        public int Wins { get; set; }
        public int Grenaded { get; set; }
    }
}
