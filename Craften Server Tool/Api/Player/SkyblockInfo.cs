﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Craften_Server_Tool.Api.Player {
    public class SkyblockInfo {
        public List<Island> Islands { get; set; }
        public Dictionary<String, Challenge> Challenges { get; set; }
    }
}
