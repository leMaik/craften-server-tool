﻿namespace Craften_Server_Tool.Api.Player {
    public class McMmoSkills {
        public int Acrobatics { get; set; }
        public int Alchemy { get; set; }
        public int Archery { get; set; }
        public int Axes { get; set; }
        public int Excavation { get; set; }
        public int Fishing { get; set; }
        public int Herbalism { get; set; }
        public int Mining { get; set; }
        public int Repair { get; set; }
        public int Smelting { get; set; }
        public int Swords { get; set; }
        public int Taming { get; set; }
        public int Unarmed { get; set; }
        public int Woodcutting { get; set; }
    }
}
