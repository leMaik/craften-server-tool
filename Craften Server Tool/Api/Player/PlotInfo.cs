﻿using System.Collections.Generic;

namespace Craften_Server_Tool.Api.Player {
    public class PlotInfo {
        public List<Plot> Plots { get; set; }
        public List<Plot> AllowedPlots { get; set; }
    }
}
