﻿using RestSharp.Deserializers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Craften_Server_Tool.Api.Player {
    public class Challenge {
        [DeserializeAs(Name = "displayName")]
        public String Name { get; set; }
        public int TimesCompleted { get; set; }
    }
}
