﻿namespace Craften_Server_Tool.Api.Player {
    public class EconomyInfo {
        public double Balance { get; set; }
        public int Rank { get; set; }
    }
}
