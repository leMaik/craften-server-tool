﻿using System;
using System.Collections.Generic;

namespace Craften_Server_Tool.Api.Player {
    public class Island {
        public String Leader { get; set; }
        public int Level { get; set; }
        public List<String> Party { get; set; }
        public Location2D Position { get; set; }
    }
}
