﻿using System.Threading.Tasks;
using RestSharp;

namespace Craften_Server_Tool.Api {
    public static partial class CraftenApi {
        private const string API_BASE = "http://craften.de/api/";

        private static T Execute<T>(RestRequest request) where T : new() {
            var client = new RestClient(API_BASE);
            var response = client.Execute<T>(request);

            if (response.ErrorException == null)
                return response.Data;

            const string message = "Error retrieving response.  Check inner details for more info.";
            throw new ApiException(message, response.ErrorException);
        }

        private static async Task<T> ExecuteAsync<T>(RestRequest request) where T : new() {
            return await TaskEx.Run(() => Execute<T>(request));
        }

        /// <summary>
        /// Liefert den die absolute URL zum angegebenen Pfad.
        /// </summary>
        /// <param name="path">Pfad mit führenden Schrägstrich</param>
        /// <returns>Die absolute URL zum angegebenen Pfad</returns>
        public static string GetAbsolutUrl(string path) {
            return API_BASE + path;
        }

        public enum FailedBehavior {
            Cancel,
            Retry
        }
    }
}
